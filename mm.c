
/*
 * mm.c
 *
 *
 * This implementation of a malloc uses an explicit list of free blocks where each
 * free block has a next and previous pointer to the next free block (doubly linked list).
 *
 * Each block has a header and a footer that contain the size of the block (header + payload + footer = block)
 * and weather the block is free or allocated.
 *
 * The list starts at our ROOT pointer. That pointer then points to the first free block given to us.
 * The HEADER block starts 12 bytes past the ROOT to give the ROOT some dummy space and
 * our FOOTER block is 11 bytes before the last byte on the heap.
 *
 * All the blocks must be at least 16 bytes with a minimum payload of 8 bytes to keep everything
 * correct.
 *
 * When we allocate a block we check if there is space available in our free list and
 * if so we put the allocated block there. If there is no space available we expand the
 * heap by CHUNKSIZE and try to look again for a free block, starting from the last free
 * block expanded to our new size.
 *
 * When a block that is allocated is freed we insert the newly freed block as the
 * first block in our free list (LIFO). Every time a block is freed we check if there
 * is an option to coalesce, if so, we coalesce in constant time.
 *
 * When realloc is called we check if the ptr is NULL, if so we only call malloc with
 * the given size. If the size is 0 we call free on the pointer and return NULL. If both
 * are valid we have to check if we can use the next free block to expand the current block
 * or if we have to shrink the current block to fit the new size. Then if all else fails
 * the last case is simply just to call malloc on the new size and free the old memory.
 *
 * Damn this was hard...
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include "mm.h"
#include "memlib.h"

/*********************************************************
 * NOTE TO STUDENTS: Before you do anything else, please
 * provide your team information in below _AND_ in the
 * struct that follows.
 *
 * Note: This comment is parsed. Please do not change the
 *       Format!
 *
 * === User information ===
 * Group: Two Pointers
 * User 1: hjalti15
 * SSN: 0908952579
 * User 2: gunnarbb15
 * SSN: 2002952429
 * User 3:
 * SSN: X
 * === End User Information ===
 ********************************************************/
team_t team = {
    /* Group name */
    "Two Pointers",
    /* First member's full name */
    "Hjalti Steinar Sigurbjörnsson",
    /* First member's email address */
    "hjalti15@ru.is",
    /* Second member's full name (leave blank if none) */
    "Gunnar Bjarki Björnsson",
    /* Second member's email address (leave blank if none) */
    "gunnarbb15@ru.is",
    /* Third full name (leave blank if none) */
    "",
    /* Third member's email address (leave blank if none) */
    ""
};

/* Constants */

/* word size (bytes) */
static const int WSIZE = 	  		4;
/* doubleword size (bytes) */
static const int DSIZE  =    		8;
/* initial heap size (bytes) */
static const int CHUNKSIZE =  		(1<<12);
/* overhead of header and footer (bytes) */
static const int OVERHEAD  =  		8;
/* Minimum block size */
static const int MIN_BLOCK =  		16;
/* single word (4) or double word (8) alignment */
static const int ALIGNMENT =  		8;
/*Header padding */
static const int HEADERPADDING = 	12;
/*Footer padding */
static const int FOOTERPADDING  = 	11;


/* Get pointer to the prev free block from the header */
static inline void* GET_PRV_PTR(void* p){
    return (p + WSIZE + WSIZE);
}

/* Get pointer to the next free block from the header */
static inline void* GET_NXT_PTR(void* p){
    return (p + WSIZE);
}
/* Set the value to p */
static inline void PUT(void* p, size_t val){
    *(size_t *)(p) = val;
}

/* Get the value at p */
static inline size_t GET(void* p){
    return (*(size_t *)(p));
}

/* Get size of block */
static inline int GET_SIZE(void* p){
    return (GET(p) & ~0x7);
}

/* Given block ptr p, compute address of next block */
static inline void* NEXT_BLKP(void* p){
    return ((char *)(p) + GET_SIZE(((char *)(p) - WSIZE)));
}

/* Given block ptr p, compute address of prev block */
static inline void* PREV_BLKP(void* p){
    return ((char *)(p) - GET_SIZE(((char *)(p) - DSIZE)));
}

/* Given a pointer to a block, get the header address */
static inline void* HDRP(void* p){
    return ((char *)(p) - WSIZE);
}

/* Given a pointer to a block, get the footer address */
static inline void* FTRP(void* p){
    return ((char *)(p) + GET_SIZE(HDRP(p)) - DSIZE);
}

/* Get if block is allocated */
static inline int GET_ALLOC(void* p){
    return (GET(p) & 0x1);
}

/* Allocate block */
static inline void ALLOCATE_BIT(void* p){
    size_t a = GET(p);
    PUT(p, (a | 1));
}

/* Free block */
static inline void FREE_BIT(void* p){
    size_t a = GET(p);
    PUT(p, (a & -2));
}


/*
 * Prints out the function that is calling and
 * Runs the mem checker
 */
static inline void CHECKHEAP(void){

    printf("\n");
    printf("caller: %s\n", __func__);
    mm_check();
}

/* Rounds up to the nearest multiple of ALIGNMENT */
static inline int ALIGN(int size){
    return (((size) + (ALIGNMENT-1)) & ~0x7);
}


/* Global pointers */
static void* ROOT = NULL;
static void* HEADER = NULL;
static void* FOOTER = NULL;

/* Functions */
int mm_check(void);
void mark_allocated(void* header_ptr);
void add_size_to_header_footer(int block_size, void* header_ptr);
void add_to_freelist(void* block, void* prev);
void remove_from_freelist(void* block);
void* expand(size_t size);
void move_to_front_of_freelist(void* ptr);
void mark_free(void* header_ptr);
int setup(void);
void coalesce(void* ptr);
void coalesce_both(void* ptr, void* next_block, void* prev_block);
void coalesce_prev(void* ptr, void* prev_block);
void coalesce_next(void* ptr, void* next_block);

/* Mem Checker functions */
static void printblock(void *bp);
int freelistisvalid();
int allfreeblocksinfreelist();
int starttoend();
void freeListCount();
void allocCount();


////////////////// MEM CHECKER //////////////////

/* Print the freelist as well as the whole heap */
void printlist() {
    // Start at the first block
    void* temp = HEADER + WSIZE;

    printf("WHOLE LIST: \n");

    // Loop until we pass the header
    while ((temp - DSIZE) <= FOOTER) {
        // If we hit the footer, break the loop
        if ((temp - DSIZE) == FOOTER) {
            break;
        }

        // Print block
        printblock(temp);

        // Iterate
        temp = NEXT_BLKP(temp);
    }

    // Start at the beginning of our freelist
    void* freeList = (void*)GET(ROOT);

    printf("FREE LIST: \n");
    // Loop until we finish the freelist
    while (freeList != NULL) {
        // Print block
        printblock(freeList);

        // Iterate
        freeList = (void*)GET(freeList);
    }
}

/* Run through all blocks free and allocated */
int starttoend()
{
    int finished = 0;

    // Start at the first first block
    void* temp = HEADER + WSIZE;

    // Loop until we have passed the Footer
	while ((temp - DSIZE) <= FOOTER) {
        // If we land on the footer, print Success and return
		if ((temp - DSIZE) == FOOTER) {
			printf("Got from start of list to end of list\n");
			return 1;
		}
        else{
            // Iterate
            temp = NEXT_BLKP(temp);
        }
	}

	return finished;
}

/* Checks if the free list only contains free blocks */
int freelistisvalid()
{
    // Start from our root
    void* temp = (void*)GET(ROOT);

	while (temp != NULL) {
        // If a block is allocated in the header or footer return false
		if (GET_ALLOC(HDRP(temp)) || GET_ALLOC(FTRP(temp))) {
            printf("The freelist is not valid!\n");
			return 0;
		}
        // Get the pointer that temps next is currently pointing to
		temp = (void*)GET(temp);
	}

	printf("The freelist is valid \n");

    return 1;
}

/* Count blocks in freelist */
void freeListCount()
{
    void* freeList = (void*)GET(ROOT);
    int freeCount = 0;

    // Run through our free list until we reach our footer
    while (freeList != NULL) {

        if (GET_ALLOC(HDRP(freeList)) || GET_ALLOC(FTRP(freeList))) {
            printf("The freelist is not valid at block nr %i!\n",freeCount);
            break;
        }
        freeCount += 1;
        freeList = (void*)GET(freeList);
    }

    printf("Free list count: %i\n",freeCount);
}

/* Count allocated blocks */
void allocCount()
{
    // Start at the first block
    void* temp = HEADER + WSIZE;
    int allocCount = 0;

    // Loop until we have passed our Footer
    while (temp < FOOTER) {
        if (GET_ALLOC(HDRP(temp)) || GET_ALLOC(FTRP(temp))) {
            allocCount += 1;
        }
        temp = NEXT_BLKP(temp);
    }

    printf("Alloc list count: %i\n",allocCount);
}

/* Checks if the free list contains all the blocks that are free */
int allfreeblocksinfreelist()
{
    // Get our pointers to start of both lists
    void* list = HEADER + WSIZE;
    void* freeList = (void*)GET(ROOT);

    // Counters
    int listFreeCount = 0, freeCount = 0;

    // Run through our list until we reach our footer
    while (list < FOOTER) {
        // If the current block is free add to our count
        if (GET_ALLOC(HDRP(list)) == 0)
            listFreeCount = listFreeCount + 1;

        // Continue into our list
        list = NEXT_BLKP(list);
    }

    // Run through our free list until we reach our footer
    while (freeList != NULL) {
        freeCount += 1;
        freeList = (void*)GET(freeList);
    }

    if (listFreeCount == freeCount) {
        printf("Free list blocks match the blocks that are free");
    }
    //if the count is equal we are good
    return (listFreeCount == freeCount);
}

/*
 * mm_check - Heap Consistency Checker.
 */
int mm_checkheap(void)
{
    printlist();

    freeListCount();

    allocCount();

    if (!starttoend()) {
        printf("Error: Not able to run from start to end\n");
        return 0;

    }
    if(!freelistisvalid()) {
        printf("Error: Free list contains an allocated block!\n");
        return 0;
    }
    if(!allfreeblocksinfreelist()) {
        printf("Error: there is a mismatch between the free list blocks and all blocks that are free\n");
        return 0;
    }

    printf("\n");

    return 1;
}


////////////////// CHECKER END //////////////////
/////////////// HELPER FUNCTIONS ////////////////

/* Move a block to the front of the freelist */
void move_to_front_of_freelist(void* ptr)
{

    // Connect the Root to our block and point our block at what the Root was pointing at
    PUT(GET_NXT_PTR(ptr), GET(GET_NXT_PTR(ROOT)));
    PUT(GET_PRV_PTR(ptr), (size_t)ROOT);

    PUT(GET_PRV_PTR((void*)GET(GET_NXT_PTR(ROOT))), (size_t)ptr);
    PUT(GET_NXT_PTR(ROOT), (size_t)ptr);
}

/* Remove a block from the freelist */
void remove_from_freelist(void* block)
{
    // Get the next and previous free blocks
    void* next = (void*)GET(GET_NXT_PTR(block));
    void* prev = (void*)GET(GET_PRV_PTR(block));

    // And connect them together, bypassing our block
    PUT(GET_PRV_PTR(next), (size_t)prev);
    PUT(GET_NXT_PTR(prev), (size_t)next);
}

/* Add a block to the freelist at a given position */
void add_to_freelist(void* block, void* prev)
{
    // Connect the block that points to prev block to our block and the
    // block that prev block points to we point our block to
    PUT(GET_NXT_PTR(block), GET(GET_NXT_PTR(prev)));
    PUT(GET_PRV_PTR(block), GET(GET_PRV_PTR(prev)));
    PUT(GET_NXT_PTR((void*)GET(GET_PRV_PTR(block))), (size_t)block);
    PUT(GET_PRV_PTR((void*)GET(GET_NXT_PTR(block))), (size_t)block);
}


/* Adds the size of the block to the header and footer */
void add_size_to_header_footer(int block_size, void* header_ptr) {
    //adds the size of the block to the header
    PUT(header_ptr, block_size);
    //adds the size of the block to the footer
    void* footer = FTRP(header_ptr + WSIZE);
    PUT(footer, block_size);
}

/* Allocates both the header and the footer of a block */
void mark_allocated(void* header_ptr) {
    //allocate the header
    ALLOCATE_BIT(header_ptr);
    //allocate the footer
    void* footer = FTRP(header_ptr + WSIZE);
    ALLOCATE_BIT(footer);
}

/* Mark the header and footer as free */
void mark_free(void* header_ptr) {
    // Free the header
    FREE_BIT(header_ptr);
    // Free the footer
    FREE_BIT(FTRP(header_ptr + WSIZE));
}

/////////// HELPER ENDS ////////////////
///////////// MALLOC ///////////////////

/*
 * mm_init - initialize the malloc package.
 */
int mm_init(void)
{
    return setup();
}

/* Sets up our header, footer and root, get heap memory */
int setup(void){

    //Initialize the heap as 2048 bytes and assigning the first block to the
    //ROOT pointer
    if ((ROOT = mem_sbrk(CHUNKSIZE)) == NULL) {
        return -1;
    }

    //Put header at beginning of list, with 12 bytes offset
    HEADER = ROOT + HEADERPADDING;

    // Make ROOT hold the size of the heap
    PUT(ROOT, CHUNKSIZE);

    // Make ROOT next_pointer point at header
    PUT(GET_NXT_PTR(ROOT), (size_t)HEADER);

    // Initialize the Header and footer
    add_size_to_header_footer(CHUNKSIZE - (2 * HEADERPADDING), HEADER);

    // Make FOOTER point at the last footer of the list
    FOOTER = mem_heap_hi() - FOOTERPADDING;

    // Make FOOTER prev_pointer point at header
    PUT(GET_PRV_PTR(FOOTER), (size_t)HEADER);

    // Connect the header to the footer and the header to the root
    PUT(GET_NXT_PTR(HEADER), (size_t)FOOTER);
    PUT(GET_PRV_PTR(HEADER), (size_t)ROOT);

    return 0;
}


/*
 * mm_malloc - Allocate a block by incrementing the brk pointer.
 * Always allocate a block whose size is a multiple of the alignment.
 */
void *mm_malloc(size_t size)
{
    //if it is an invalid size return NULL immediately
    if (size <= 0) {
        return NULL;
    }

    //starting point to look for space to allocate
    void* temp = (void*)GET(GET_NXT_PTR(ROOT));

    //calculate the size needed to store the information
    int real_size = ALIGN(size + OVERHEAD);

    int b_size = 0;

    for ( ; temp < FOOTER; temp = (void*)GET(GET_NXT_PTR(temp))) {
        if ((b_size = GET_SIZE(temp)) >= real_size) {
            int free_space = b_size - real_size;

            // If the block fits but no room for another block
            if (free_space < MIN_BLOCK) {
                // Remove the block from freelist
                remove_from_freelist(temp);

                // Mark as allocated
                mark_allocated(temp);

            // If there is room for another block
            } else {
                // find next free block
                void* next_free_block = temp + real_size;

                // Create the next free block
                add_size_to_header_footer(free_space, next_free_block);

                // Connect the next_free_block to the freelist
                add_to_freelist(next_free_block, temp);

                // Create the allocated block
                add_size_to_header_footer(real_size, temp);

                // Mark the block as allocated
                mark_allocated(temp);
            }

            return (temp + WSIZE);
        }
    }

    return expand(size);
}

/*
 * expand - expands the heap so there is room for the malloc call
 */
void* expand(size_t size)
{
    // Find the actual needed size
    int real_size = ALIGN(size + OVERHEAD);

    // Initialize the size to be added as CHUNKSIZE
    int size_to_add = CHUNKSIZE;

    // Expand until size_to_add is bigger than size
    while (real_size > size_to_add) {
        size_to_add = size_to_add + CHUNKSIZE;
    }

    // Expand the heap and check if everything executes correctly
    if (mem_sbrk(size_to_add) == NULL) {
        return NULL;
    }

    // Store the old footer
    void* old_footer = FOOTER;

    // Assign the old footer to the end of the list
    FOOTER = mem_heap_hi() - FOOTERPADDING;

    // Get the last block
    void* last_block = HDRP(PREV_BLKP(old_footer + WSIZE));

    // Check if the last block is allocated
    if (GET_ALLOC(last_block)) {
        // Create the block
        add_size_to_header_footer(size_to_add, old_footer);

        // Connect the old footer and the new FOOTER
        PUT(GET_NXT_PTR(old_footer), (size_t)FOOTER);
        PUT(GET_PRV_PTR(FOOTER), (size_t)old_footer);
    } else {
        // Remove the old last block from the free list
        remove_from_freelist(last_block);

        // Connect next and prev pointer of the old block
        PUT(GET_PRV_PTR(last_block), GET(GET_PRV_PTR(old_footer)));
        PUT(GET_NXT_PTR((void*)GET(GET_PRV_PTR(old_footer))), (size_t)last_block);
        // Make the footer point at the last block and vice versa
        PUT(GET_PRV_PTR(FOOTER), (size_t)last_block);
        PUT(GET_NXT_PTR(last_block), (size_t)FOOTER);

        // Get the size of the new block
        int new_size = GET_SIZE(last_block) + size_to_add;

        // Add the size to the header and footer
        add_size_to_header_footer(new_size, last_block);
    }
    // Call malloc to finish the deal
    return mm_malloc(size);
}

/*
 * mm_free - Free the block and coalesce.
 */
void mm_free(void *ptr)
{
    coalesce(ptr);
}

/*
 * Coalesce blocks when free is called
 */
void coalesce(void* ptr)
{
    // Get the previous and next block
    void* prev_block = HDRP(PREV_BLKP(ptr));
    void* next_block = HDRP(NEXT_BLKP(ptr));

    // Check if they are allocated
    int prev_allocated = GET_ALLOC(prev_block);
    int next_allocated = GET_ALLOC(next_block);

    // If the pointer is not the first block and the previous block is not allocated
    if ((HDRP(ptr) != ROOT) && !prev_allocated) {
        // We remove the previous block from our freelist for coalesce
        remove_from_freelist(prev_block);

        // If the block is not the last block on the heap and the next block is not allocated
        if ((HDRP(ptr) != HDRP(PREV_BLKP(FOOTER + WSIZE))) && !next_allocated) {
            // Coalesce both blocks
            coalesce_both(ptr, next_block, prev_block);

        // If the next block is allocated
        } else {
            // Coalesce prev block
            coalesce_prev(ptr, prev_block);
        }

    // If the block being freed is not the last block on the heap and the next block
    // is not allocated
    } else if ((HDRP(ptr) != HDRP(PREV_BLKP(FOOTER + WSIZE))) && !next_allocated) {
        // Coalesce next block
        coalesce_next(ptr, next_block);

    // If both the previous and next block are allocated
    } else {
        // We mark our block as free
        mark_free(HDRP(ptr));

        // And add it to the front of the freelist
        move_to_front_of_freelist(HDRP(ptr));
    }
}

/*
 * Coalesce both the prev block and next block with the current block
 */
void coalesce_both(void* ptr, void* next_block, void* prev_block)
{
    // We remove the next block from our freelist for coalesce
    remove_from_freelist(next_block);

    // Get the size of all the blocks that are about to be coalesced
    size_t new_size = GET_SIZE(prev_block) + GET_SIZE(HDRP(ptr)) + GET_SIZE(next_block);

    // Create the header and footer for the new block
    add_size_to_header_footer(new_size, prev_block);

    // Add the block to the front of our freelist
    move_to_front_of_freelist(prev_block);
}

/*
 * Coalesce the prev block with current block
 */
void coalesce_prev(void* ptr, void* prev_block)
{
    // Get the size of the previous block + size of the block being freed
    size_t new_size = GET_SIZE(prev_block) + GET_SIZE(HDRP(ptr));

    // Create the header and footer for coalesced block
    add_size_to_header_footer(new_size, prev_block);

    // Add the block to the front of our freelist
    move_to_front_of_freelist(prev_block);
}

/*
 * Coalesce the next block with current block
 */
void coalesce_next(void* ptr, void* next_block)
{
    // Remove the next block from our freelist
    remove_from_freelist(next_block);

    // Get the size of next block + size of block being freed
    size_t new_size = GET_SIZE(next_block) + GET_SIZE(HDRP(ptr));

    // Create header and footer for new coalesced block
    add_size_to_header_footer(new_size, HDRP(ptr));

    // Move new block to the front of the freelist
    move_to_front_of_freelist(HDRP(ptr));
}

/*
 * mm_realloc - reallocates a block with a new size that has been allocated before.
 */
void *mm_realloc(void *ptr, size_t size)
{
    //call mm_free with ptr if size = 0
    if (size == 0) {
        mm_free(ptr);
        return NULL;
    }

    //return malloc with size if ptr = null
    if (ptr == NULL) {
        return mm_malloc(size);
    }

    // Get the size of the ptr, discarding the header and footer
    size_t old_size = GET_SIZE(HDRP(ptr)) - OVERHEAD;

    // If the size is the same as before, do nothing and return the ptr
    if (size == old_size) {
        return ptr;
    }

    // Get the aligned size
    int real_size = ALIGN(size + OVERHEAD);

    // If the size is smaller than the original block
    if (real_size < GET_SIZE(HDRP(ptr))) {
        // Find the difference between the old block and the new size
        int difference = (old_size + OVERHEAD) - real_size;

        // If the difference between the old size and the requested size is less than
        // 16 we don't want to do anything so we just return the pointer, even though
        // it causes some fragmentation
        if (difference < MIN_BLOCK) {
            return ptr;
        }

        // Create the allocated block
        add_size_to_header_footer(real_size, HDRP(ptr));
        mark_allocated(HDRP(ptr));

        // Create a block from the difference (that is bigger than MIN_BLOCK) and put
        // it at the front of the freelist
        void* new_block = HDRP(ptr) + real_size;
        add_size_to_header_footer(difference, new_block);
        move_to_front_of_freelist(new_block);

        return ptr;
    }

    // Get the next block
    void* next_block = HDRP(NEXT_BLKP(ptr));

    // Check if its allocated
    int next_block_alloc = GET_ALLOC(next_block);

    // Get the difference between two blocks combined size and the actual size
    // of the realloc request
    int difference = (GET_SIZE(HDRP(ptr)) + GET_SIZE(next_block)) - real_size;

    // Check to see if the next block is not the FOOTER
    if (next_block != FOOTER) {
        // Check to see that the next block is not allocated and the difference is
        // bigger than 0
        if (!next_block_alloc && difference >= 0) {
            // If the difference is smaller than MIN_BLOCK we will remove the next
            // block from our freelist and accept a small fragmentation
            if (difference < MIN_BLOCK) {
                remove_from_freelist(next_block);
                add_size_to_header_footer((GET_SIZE(HDRP(ptr)) + GET_SIZE(next_block)), HDRP(ptr));
                mark_allocated(HDRP(ptr));
                return ptr;
            }

            // Create the free block from the remaining bytes and add it to the free list
            void* free_block = HDRP(ptr) + real_size;
            add_to_freelist(free_block, next_block);
            add_size_to_header_footer(difference, free_block);

            // Create the reallocated block and return the address to the payload
            add_size_to_header_footer(real_size, HDRP(ptr));
            mark_allocated(HDRP(ptr));

            return ptr;
        }
    }

    // If there is not space for our reallocate we have to call mm_malloc again and copy the payload to
    // that block and than free the old block
    void* new_block = mm_malloc(size);
    memcpy(new_block, ptr, old_size);
    mm_free(ptr);

    return new_block;
}

/* Print a given block */
static void printblock(void *bp)
{
    size_t hsize, halloc, fsize, falloc;

    hsize = GET_SIZE(HDRP(bp));
    halloc = GET_ALLOC(HDRP(bp));
    fsize = GET_SIZE(FTRP(bp));
    falloc = GET_ALLOC(FTRP(bp));

    if (hsize == 0) {
        printf("%p: EOL\n", bp);
        return;
    }

    printf("%p: header: [%zu:%c] footer: [%zu:%c]\n", bp,
           hsize, (halloc ? 'a' : 'f'),
           fsize, (falloc ? 'a' : 'f'));
}
